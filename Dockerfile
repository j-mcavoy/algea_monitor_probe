FROM python:3.8


RUN mkdir -p /srv/app/probe
WORKDIR /srv/app/probe

CMD python probe.py
